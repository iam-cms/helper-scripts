#!/usr/bin/env bash

# Stop the script on any errors
set -e

# Update and upgrade the system
sudo apt-get update
sudo apt-get upgrade -y

# Install pip
sudo apt install python3-pip -y

# Set variables
echo 'export PATH=${HOME}/.local/bin:${PATH}' >> ${HOME}/.bashrc
echo 'export DISPLAY=:0.0' >> ${HOME}/.bashrc

# Install kadi-apy and workflow nodes
pip3 install kadi-apy
pip3 install workflow-nodes

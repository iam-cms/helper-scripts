import matplotlib.pyplot as plt
import scienceplots
import pandas as pd
import numpy as np
import csv

import re
import dateutil.parser as date_parser
import os

from matplotlib.animation import FuncAnimation
from matplotlib.animation import PillowWriter
from IPython.display import Markdown as md


# Kadi4mat ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import kadi_apy
from kadi_apy import KadiManager

manager = KadiManager()


def encoded_in(filepath):
    encodings_to_try = ["utf-8", "iso-8859-1", "windows-1252", "utf-16", "utf-32"]

    for encoding in encodings_to_try:
        try:
            with open(filepath, "r", encoding=encoding) as f:
                files = f.read()
                # print(f'Input file encoding is: {encoding}')
                encoded_in = encoding
            # If the decoding was successful, break out of the loop
            break
        except UnicodeDecodeError:
            continue  # Skip the "Failed to decode" message for this encoding

    if files is None:
        raise Exception("Unable to decode the file with any of the specified encodings")

    try:
        with open(filepath, "r", encoding=encoding) as f:
            files = f.read()
            # print(files[0])
    except UnicodeDecodeError as e:
        print(f"UnicodeDecodeError: {e}")

    return encoded_in


def extract_metadata(filepath):
    """
    extract metadata from the cv output file
    """
    peis_metadata = []

    with open(filepath, "r", encoding=encoding) as f:
        files = f.read().split("Cycle Definition")
    content = read_between_lines(filepath, start_line, end_line)

    def extract_parameter(pattern_str, key, unit, value_type):
        regex_search = re.findall(pattern_str, content, re.I)
        parameter_value = regex_search[0].strip() if regex_search else None
        try:
            if parameter_value is not None:
                parameter_value = value_type(parameter_value)
        except ValueError:
            print(
                f"Warning: Unable to convert '{parameter_value}' to {value_type.__name__} for key '{key}'"
            )

        return {
            "key": key,
            "type": value_type.__name__,
            "unit": unit,
            "value": parameter_value,
        }
        # return {"key": key, "type": value_type, "value": parameter_value}

    parameter_patterns = [
        (
            "Mass of active material\s*:\s+\s*(\S+.?)",
            "Mass of active material",
            "mg",
            float,
        ),
        ("at\s+x\s+=\s*:\s*\s+\s*(\S+.?)", "at x =", None, float),
        (
            "Molecular weight of active material \(at x = 0\)\s*\:\s*(\S+.?)",
            "Molecular weight of active material (at x = 0)",
            "g/mol",
            float,
        ),
        (
            "Atomic weight of intercalated ion\s*:\s+\s*(\S+.?)",
            "Atomic weight of intercalated ion",
            "g/mol",
            float,
        ),
        (
            "Acquisition started at: x0 =\s*:\s+\s*(\S+.?)",
            "Acquisition started at: x0 =",
            None,
            float,
        ),
        (
            "Number of e- transfered per intercalated ion\s*:\s+\s*(\S+.?)",
            "Number of e- transfered per intercalated ion",
            None,
            float,
        ),
        ("for DX = 1, DQ =\s*:\s+\s*(\S+.?)", "for DX = 1", "mA.h", float),
        ("Battery capacity\s*:\s+\s*(\S+.?)", "Battery capacity", "mA.h", float),
        (
            "Electrode surface area\s*:\s+\s*(\S+.?)",
            "Electrode surface area",
            "cm²",
            float,
        ),
        ("Characteristic mass\s*:\s+\s*(\S+.?)", "Characteristic mass", "mg", float),
        ("Volume \(V\)\s*:\s+\s*(\S+.?)", "Volume (V)", "cm³", float),
    ]

    for pattern, key, unit, value_type in parameter_patterns:
        peis_metadata.append(extract_parameter(pattern, key, unit, value_type))
    for dictionary in peis_metadata:
        for key, value in dictionary.items():
            if value == "null":
                dictionary[key] = None

    return peis_metadata


def read_between_lines(file_path, start_line, end_line):
    with open(filepath, "r", encoding=encoding) as file:
        content_between_lines = []
        for current_line_number, line in enumerate(file, start=1):
            if start_line <= current_line_number <= end_line:
                # Append lines between start_line and end_line to the list
                content_between_lines.append(line.strip())
            elif current_line_number > end_line:
                # Break out of the loop once we've read the desired lines
                break

    content = "\n".join(content_between_lines)

    return content


def extract_metadata_table_between_words(filepath, start_word, end_word):
    """
    extract metadata as a table and save it as a csv file
    """
    result_lines = []
    encoding = encoded_in(filepath)

    try:
        with open(filepath, "r", encoding=encoding) as file:
            # Flag to indicate whether we are between start_word and end_word
            between_words = False

            for line in file:
                # Check if the line contains the start_word
                if start_word in line:
                    between_words = True

                # If we are between start_word and end_word, add the line to the result
                if between_words:
                    result_lines.append(line)

                # Check if the line contains the end_word
                if end_word in line:
                    between_words = False
                    break  # Stop reading the file after the first occurrence of end_word
        # Write result_lines to a CSV file
        result_lines = result_lines[:-2]
        directory_path = "/".join(filepath.split("/")[:-1])
        output_metadata_table_path = directory_path + "/metadata_cv.csv"
        with open(output_metadata_table_path, "w", newline="") as csv_file:
            csv_writer = csv.writer(csv_file)
            for line in result_lines:
                csv_writer.writerow(
                    [line.strip()]
                )  # Write each line as a single-column row

    except FileNotFoundError:
        print(f"Error: File not found at {filepath}")
    except Exception as e:
        print(f"An error occurred: {e}")

    return result_lines


def extract_table(filepath, plot=True):
    """
    extract, plot and save the plot
    x - axis : <I>/mA
    y - axis : Ewe/V
    """

    directory_path = os.path.join(*filepath.split(os.path.sep)[:-1])
    os.makedirs(directory_path, exist_ok=True)
    output_csv_path = os.path.join(directory_path, "data_cv.csv")
    with open(filepath, "r", encoding=encoding) as file:
        df = pd.read_csv(file, sep="\t", skiprows=92)
        df.to_csv(output_csv_path, index=False)

    if plot:
        print("plot started")
        fig = plt.figure(figsize=(8, 6))
        plt.style.use(["science", "notebook"])
        plt.plot(df["<I>/mA"], df["Ewe/V"], "-", linewidth=0.5)
        plt.xlabel("I/mA")
        plt.ylabel("Ewe/V")
        fig.savefig("plot_cv.png", dpi=300, bbox_inches="tight")

    return df


def kadi_export():
    """
    upload extracted metadata, metadata table, data file and plot to kadi4mat
    """
    extract_metadata_table_between_words(filepath, start_word, end_word)
    read_between_lines(filepath, start_line, end_line)
    cv_metadata = extract_metadata(filepath)
    print(cv_metadata)
    df = extract_table(filepath, plot=True)

    record = manager.record(
        identifier=identifier, title=title, create=True, extras=cv_metadata
    )
    plot_cv = "plot_cv.png"
    data_cv = "data_cv.csv"
    metadata_cv = "metadata_cv.csv"
    record.upload_file(os.path.join(os.getcwd(), plot_cv))
    record.upload_file(os.path.join(os.getcwd(), data_cv))
    record.upload_file(os.path.join(os.getcwd(), metadata_cv))


# USER INPUTS START -----------------------------------------------------------------------------------
# file locations, specify the path of results file
filepath = "pathofthefile"

# start_word and end_word refer to the starting and ending words
# in the results file to captute extra metadata that is needed as a table
start_word = "Ei (V)"
end_word = "Modify"

# start_line and end_line represent extra metadata location in the results file
start_line = 1
end_line = 45

# record specific information - kadi4mat

identifier = "record-identifier"
title = "record-title"
# USER INPUTS END -----------------------------------------------------------------------------------


encoding = encoded_in(filepath)

if __name__ == "__main__":
    kadi_export()

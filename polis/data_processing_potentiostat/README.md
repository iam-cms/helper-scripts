# Data Processing Scripts for Potentiostat measurements

These Python scripts are designed for processing cyclic voltammetry (CV), PEIS,
GCPL data and exporting relevant information to Kadi4Mat.

## Table of Contents

- [General Information](#general-information)
- [Dependencies](#dependencies)
- [Usage](#usage)
- [User Inputs](#user-inputs)
- [Functions](#functions)
  - [encoded_in(filepath)](#encoded_infilepath)
  - [extract_metadata(filepath)](#extract_metadatafilepath)
  - [read_between_lines(file_path, start_line,
    end_line)](#read_between_linesfile_path-start_line-end_line)
  - [extract_metadata_table_between_words(filepath, start_word,
    end_word)](#extract_metadata_table_between_wordsfilepath-start_word-end_word)
  - [extract_table(filepath, plot=True)](#extract_tablefilepath-plottrue)
  - [kadi_export()](#kadi_export)
- [Kadi4Mat Export](#kadi4mat-export)

## General Information

This script is designed to perform the following tasks:
- Extract metadata from a CV/GCPL/PEIS output file
- Generate metadata tables
- Extract and plot CV/GCPL/PEIS data
- Export relevant data and plots to Kadi4Mat

## Dependencies

Install the required dependencies using:

```bash
pip install matplotlib scienceplots pandas numpy opencv-python kadi-apy python-dateutil
```

## Usage

```
python cv.py
```

## User Inputs

Adjust the following parameters in the script to suit your specific use case:

* **filepath**: Path to the CV data file.
* **start_word and end_word**: Keywords to capture extra metadata in the
  results file.
* **start_line and end_line**: Line numbers indicating extra metadata location
  in the results file.
* **identifier and title**: Record-specific information for Kadi4Mat export.

## Functions

* **encoded_in(filepath)** : to find the encoding of the .txt file.
* **extract_metadata(filepath)** : extract important metadata from the .txt
  file.
* **read_between_lines(file_path, start_line, end_line)** : General function to
  capture lines between line numbers.
* **extract_metadata_table_between_words(filepath, start_word, end_word)** :
  extract metadata between start and end words.
* **extract_table(filepath, plot=True)** : extract some metadata as a table.

## Kadi4Mat export

The scripts includes a function (kadi_export) to upload extracted metadata,
metadata tables, data files, and plots to Kadi4Mat. Adjust the identifier and
title parameters for your specific use case.

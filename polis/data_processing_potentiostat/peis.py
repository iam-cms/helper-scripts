import matplotlib.pyplot as plt
import scienceplots
import pandas as pd
import numpy as np

import re
import dateutil.parser as date_parser
import os


from matplotlib.animation import FuncAnimation
from matplotlib.animation import PillowWriter
from IPython.display import Markdown as md

import cv2

# Kadi4mat ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import kadi_apy
from kadi_apy import KadiManager

manager = KadiManager()


def encoded_in(filepath):
    """
    function to identify the input file encoding
    """

    encodings_to_try = ["utf-8", "iso-8859-1", "windows-1252", "utf-16", "utf-32"]

    for encoding in encodings_to_try:
        try:
            with open(filepath, "r", encoding=encoding) as f:
                files = f.read()
                # print(f'Input file encoding is: {encoding}')
                encoded_in = encoding
            # If the decoding was successful, break out of the loop
            break
        except UnicodeDecodeError:
            continue  # Skip the "Failed to decode" message for this encoding

    if files is None:
        raise Exception("Unable to decode the file with any of the specified encodings")

    try:
        with open(filepath, "r", encoding=encoding) as f:
            files = f.read()
            # print(files[0])
    except UnicodeDecodeError as e:
        print(f"UnicodeDecodeError: {e}")

    return encoded_in


def extract_metadata(filepath):
    """
    extract extra metadata from the peis output file
    """
    peis_metadata = []

    with open(filepath, "r", encoding=encoding) as f:
        files = f.read().split("freq/Hz")
    content = read_between_lines(filepath, start_line, end_line)

    def extract_parameter(pattern_str, key, unit, value_type):
        # regex_search = re.findall(pattern_str, content, re.I)
        regex_search = re.findall(pattern_str, content)
        parameter_value = regex_search[0].strip() if regex_search else None
        try:
            if parameter_value is not None:
                parameter_value = value_type(parameter_value)
        except ValueError:
            print(
                f"Warning: Unable to convert '{parameter_value}' to {value_type.__name__} for key '{key}'"
            )

        return {
            "key": key,
            "type": value_type.__name__,
            "unit": unit,
            "value": parameter_value,
        }
        # return {"key": key, "type": value_type, "value": parameter_value}

    parameter_patterns = [
        ("Mode\s*\s+\s*(\S+.*)", "Mode", None, str),
        ("E\s+\(V\)\s*\s+\s*(\S+.*)", "E", "V", float),
        ("vs.\s{2}\s*\s+\s*(\S+.*)", "vs.", None, str),
        ("tE\s+\(h:m:s\)\s*\s+\s*(\S+.*)", "tE (h:m:s)", None, str),
        ("record\s*\s+\s*(\S+.*)", "record", None, float),
        ("unit\s+dI\s*\s+\s*(\S+.*)", "unit dI", None, str),
        ("dt\s+\(s\)\s*\s+\s*(\S+.*)", "dt", None, float),
        ("fi\s*\s+\s*(\S+.*)", "fi", "MHz", float),
        ("ff\s*\s+\s*(\S+.*)", "ff", "mHz", float),
        ("^Nd\s*\s+\s*(\S+.*)", "Nd", None, int),
        ("NT\s*\s+\s*(\S+.*)", "NT", None, int),
        ("dt\s+\(mV\)\s*\s+\s*(\S+.*)", "Va", "s", float),
        ("pw\s*\s+\s*(\S+.*)", "pw", None, float),
        ("Na\s*\s+\s*(\S+.*)", "Na", None, int),
        ("Drift\s+correction\s*\s+\s*(\S+.*)", "Drift Correction", None, bool),
        ("E\s+range\s+min\s+\(V\)\s*\s+\s*(\S+.*)", "E range min", "V", float),
        ("E\s+range\s+max\s+\(V\)\s*\s+\s*(\S+.*)", "E range max", "V", float),
        ("I\s+range\s*\s+\s*(\S+.*)", "I range", None, str),
        ("Bandwidth\s*\s+\s*(\S+.*)", "Bandwidth", None, int),
        ("nc\s+cycles\s*\s+\s*(\S+.*)", "nc cycles", None, int),
        ("goto\s+Ns\s*\s+\s*(\S+.*)", "goto Ns", None, int),
        ("nr\s+cycles\s*\s+\s*(\S+.*)", "nr cycles", None, int),
    ]

    for pattern, key, unit, value_type in parameter_patterns:
        peis_metadata.append(extract_parameter(pattern, key, unit, value_type))
    for dictionary in peis_metadata:
        for key, value in dictionary.items():
            if value == "null":
                dictionary[key] = None

    return peis_metadata


def read_between_lines(file_path, start_line, end_line):
    with open(filepath, "r", encoding=encoding) as file:
        content_between_lines = []

        for current_line_number, line in enumerate(file, start=1):
            if start_line <= current_line_number <= end_line:
                # Append lines between start_line and end_line to the list
                content_between_lines.append(line.strip())
            elif current_line_number > end_line:
                # Break out of the loop once we've read the desired lines
                break

    content = "\n".join(content_between_lines)

    return content


def extract_table(filepath, plot=True):
    """
    extract, plot and save the plot
    x - axis : -Im(Z)/Ohm
    y - axis : Re(Z)/Ohm
    """

    directory_path = os.path.join(*filepath.split(os.path.sep)[:-1])
    os.makedirs(directory_path, exist_ok=True)
    output_csv_path = os.path.join(directory_path, "data_peis.csv")
    with open(filepath, "r", encoding=encoding) as file:
        df = pd.read_csv(file, sep="\t", skiprows=75)
        df.to_csv(output_csv_path, index=False)

    if plot:
        print("plot started")
        fig = plt.figure(figsize=(8, 6))
        plt.style.use(["science", "notebook"])
        plt.plot(df["-Im(Z)/Ohm"], df["Re(Z)/Ohm"], "o", linewidth=0.5)
        plt.xlabel("-Im(Z)/Ohm")
        plt.ylabel("Re(Z)/Ohm")
        plt.xlim(min(df["Re(Z)/Ohm"]), max(df["Re(Z)/Ohm"]))
        plt.ylim(min(df["Re(Z)/Ohm"]), max(df["Re(Z)/Ohm"]))
        fig.savefig("plot_peis.png", dpi=300, bbox_inches="tight")

    return df


def kadi_export():
    """
    upload extracted metadata, data file and plot to kadi4mat
    """
    read_between_lines(filepath, start_line, end_line)
    peis_metadata = extract_metadata(filepath)
    print(peis_metadata)
    extract_table(filepath)
    record = manager.record(
        identifier=identifier, title=title, create=True, extras=peis_metadata
    )
    plot_peis = "plot_peis.png"
    data_peis = "data_peis.csv"
    record.upload_file(os.path.join(os.getcwd(), plot_peis))
    record.upload_file(os.path.join(os.getcwd(), data_peis))


# USER INPUTS START -----------------------------------------------------------------------------------
# file locations, specify the path of results file
filepath = "pathofthefile"

# start_line and end_line represent extra metadata location in the results file
start_line = 5
end_line = 75

# record specific information - kadi4mat

identifier = "record-identifier"
title = "record-title"

# USER INPUTS END -----------------------------------------------------------------------------------


encoding = encoded_in(filepath)

if __name__ == "__main__":
    kadi_export()
